package email

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"mime"
	"net/mail"
	"net/smtp"
	"path/filepath"
	"strings"
	"time"
)

func sendMessageBySmtp(emailer *Emailer) error {
	if emailer == nil {
		return errors.New("sendMessageBySmtp(): emailer is a null pointer")
	}

	from, err := mail.ParseAddress(emailer.From)
	if err != nil {
		return err
	}

	// Other recipients.
	to := make([]string, len(emailer.To))
	toBody := make([]string, len(emailer.To))
	for i := 0; i < len(emailer.To); i++ {
		address, err := mail.ParseAddress(emailer.To[i])
		if err != nil {
			return err
		}
		to[i] = address.Address
		toBody[i] = address.String()
	}

	// Carbon copies.
	cc := make([]string, len(emailer.Cc))
	ccBody := make([]string, len(emailer.Cc))
	for i := 0; i < len(emailer.Cc); i++ {
		address, err := mail.ParseAddress(emailer.Cc[i])
		if err != nil {
			return err
		}
		cc[i] = address.Address
		ccBody[i] = address.String()
	}

	// Blind carbon copies.
	bcc := make([]string, len(emailer.Bcc))
	for i := 0; i < len(emailer.Bcc); i++ {
		address, err := mail.ParseAddress(emailer.Bcc[i])
		if err != nil {
			return err
		}
		bcc[i] = address.Address
	}

	body, err := emailer.renderTemplate()
	if err != nil {
		return err
	}

	// SMTP authentication.
	auth := smtp.PlainAuth("", emailer.Parameter.Smtp.Username, emailer.Parameter.Smtp.Password, emailer.Parameter.Smtp.Host)

	// Builds SMTP body.
	var message bytes.Buffer
	message.WriteString("From: " + from.String() + "\r\n")
	message.WriteString("Subject: " + emailer.Subject + "\r\n")
	message.WriteString("MIME-Version: 1.0\r\n")
	message.WriteString("To: " + strings.Join(toBody, ";") + "\r\n")
	if len(emailer.Cc) > 0 {
		message.WriteString("Cc: " + strings.Join(ccBody, ";") + "\r\n")
	}
	if len(emailer.Attachments) > 0 { // Has attachments.
		// Get a random unsigned integer as boundary.
		boundary := generateBoundary()

		message.WriteString("Content-Type: multipart/mixed; boundary=\"" + boundary + "\"\r\n")
		message.WriteString("Content-Description: Email with attachments\r\n\r\n")

		// Email text body.
		message.WriteString("--" + boundary + "\r\n")
		message.WriteString("Content-Type: text/html; charset=utf-8\r\n\r\n")
		message.WriteString(body)
		message.WriteString("\r\n\r\n")

		// Adds attachments.
		for _, attachment := range emailer.Attachments {
			attachmentBody, err := buildAttachment(attachment, boundary)
			if err != nil {
				return err
			}
			message.Write(attachmentBody)
		}

		// End of email body.
		message.WriteString("--" + boundary + "--\r\n\r\n")
	} else { // Without attachment.
		message.WriteString("Content-Type: text/html; charset=UTF-8\r\n\r\n")
		message.WriteString(body)
		message.WriteString("\r\n\r\n")
	}

	recipients := append(to, cc...)
	recipients = append(recipients, bcc...)

	smtpServer := fmt.Sprintf("%s:%d", emailer.Parameter.Smtp.Host, emailer.Parameter.Smtp.Port)

	if err := smtp.SendMail(smtpServer, auth, from.Address, recipients, message.Bytes()); err != nil {
		return err
	}

	return nil
}

func buildAttachment(file string, boundary string) ([]byte, error) {
	// Parse file information.
	path, err := filepath.Abs(file)
	if err != nil {
		return nil, err
	}
	ext := filepath.Ext(path)
	mimeType := mime.TypeByExtension(ext)
	_, name := filepath.Split(path)

	var message bytes.Buffer
	message.WriteString("--" + boundary + "\r\n")
	message.WriteString("Content-Type: " + mimeType + "\r\n")
	message.WriteString("Content-Description: A " + ext + " file\r\n")
	message.WriteString("Content-Transfer-Encoding: base64\r\n")
	message.WriteString("Content-Disposition: attachment; filename=\"" + name + "\"\r\n\r\n")

	// Read file content.
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	encodedData := base64.StdEncoding.EncodeToString(data)
	message.WriteString(encodedData)
	message.WriteString("\r\n\r\n")

	return message.Bytes(), nil
}

func generateBoundary() string {
	rand.Seed(time.Now().UnixNano())
	x := rand.Uint64()
	boundary := fmt.Sprintf("%x", x)
	return boundary
}
