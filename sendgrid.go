package email

import (
	"encoding/base64"
	"errors"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"io/ioutil"
	"log"
	"mime"
	address "net/mail"
	"path/filepath"
)

func sendMessageToSendgrid(emailer *Emailer) error {
	if emailer == nil {
		return errors.New("sendMessageToSendgrid(): emailer is a null pointer")
	}

	from, err := parseEmail(emailer.From)
	if err != nil {
		return err
	}

	body, err := emailer.renderTemplate()
	if err != nil {
		return err
	}
	html := mail.NewContent("text/html", body)

	message := mail.NewV3Mail()
	message.SetFrom(from)
	message.AddContent(html)
	message.Subject = emailer.Subject

	personalization := &mail.Personalization{}

	// Add other recipients.
	for _, to := range emailer.To {
		email, err := parseEmail(to)
		if err != nil {
			return err
		}
		personalization.AddTos(email)
	}

	// Add carbon copies.
	for _, cc := range emailer.Cc {
		email, err := parseEmail(cc)
		if err != nil {
			return err
		}
		personalization.AddTos(email)
	}

	// Add blind carbon copies.
	for _, bcc := range emailer.Bcc {
		email, err := parseEmail(bcc)
		if err != nil {
			return err
		}
		personalization.AddTos(email)
	}

	message.AddPersonalizations(personalization)

	client := sendgrid.NewSendClient(emailer.Parameter.ApiKey)

	// Add attachments.
	for _, file := range emailer.Attachments {
		attachment, err := parseAttachment(file)
		if err != nil {
			return err
		}
		message.AddAttachment(attachment)
	}

	response, err := client.Send(message)
	if err != nil {
		return err
	}

	log.Printf("Sendgrid Status: %d, Response: %s\n", response.StatusCode, response.Body)

	return nil
}

func parseEmail(emailInfo string) (*mail.Email, error) {
	addr, err := address.ParseAddress(emailInfo)
	if err != nil {
		return nil, err
	}
	return mail.NewEmail(addr.Name, addr.Address), nil
}

func parseAttachment(file string) (*mail.Attachment, error) {
	// Parse file information.
	path, err := filepath.Abs(file)
	if err != nil {
		return nil, err
	}
	ext := filepath.Ext(path)
	mimeType := mime.TypeByExtension(ext)
	_, name := filepath.Split(path)

	// Read file content.
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	encodedData := base64.StdEncoding.EncodeToString(data)

	attachment := mail.NewAttachment()
	attachment.SetContent(encodedData)
	attachment.SetType(mimeType)
	attachment.SetFilename(name)
	attachment.SetDisposition("Attachment " + name)
	attachment.SetContentID("Attachment " + name)
	return attachment, nil
}
