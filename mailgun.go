package email

import (
	"context"
	"errors"
	"github.com/mailgun/mailgun-go/v3"
	"log"
	"time"
)

func sendMessageToMailgun(emailer *Emailer) error {
	if emailer == nil {
		return errors.New("sendMessageToMailgun(): emailer is a null pointer")
	}

	mail := mailgun.NewMailgun(emailer.Parameter.Domain, emailer.Parameter.ApiKey)

	body, err := emailer.renderTemplate()
	if err != nil {
		return err
	}

	message := mail.NewMessage(emailer.From, emailer.Subject, "", emailer.To[0])
	message.SetHtml(body)

	// Add other recipients.
	if len(emailer.To) > 1 {
		for _, to := range emailer.To[1:len(emailer.To)] {
			if err := message.AddRecipient(to); err != nil {
				return err
			}
		}
	}

	// Add carbon copies.
	if len(emailer.Cc) > 0 {
		for _, cc := range emailer.Cc {
			message.AddCC(cc)
		}
	}

	// Add blind carbon copies.
	if len(emailer.Bcc) > 0 {
		for _, bcc := range emailer.Bcc {
			message.AddCC(bcc)
		}
	}

	// Add attachments.
	if len(emailer.Attachments) > 0 {
		for _, attachment := range emailer.Attachments {
			message.AddAttachment(attachment)
		}
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	response, id, err := mail.Send(ctx, message)
	if err != nil {
		return err
	}

	log.Printf("Mailgun ID: %s, Response: %s\n", id, response)

	return nil
}
