module email

go 1.12

require (
	github.com/mailgun/mailgun-go/v3 v3.6.0
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.5.0+incompatible
	github.com/stretchr/testify v1.8.4 // indirect
)
