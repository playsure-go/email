package email

import (
	"bytes"
	"errors"
	"html/template"
)

type EmailPrivider uint8

const (
	EMAIL_PROVIDER_MAILGUN  EmailPrivider = 1
	EMAIL_PROVIDER_SENDGRID EmailPrivider = 2
	EMAIL_PROVIDER_SMTP     EmailPrivider = 3
)

type SmtpParameter struct {
	Username string
	Password string
	Host     string
	Port     uint16
}

type EmailParameter struct {
	Domain string
	ApiKey string
	Smtp   SmtpParameter
}

type EmailTemplate struct {
	File    string
	Name    string
	Data    interface{}
	FuncMap template.FuncMap
}

type Emailer struct {
	Parameter   EmailParameter
	Provider    EmailPrivider
	From        string
	Subject     string
	To          []string
	Cc          []string
	Bcc         []string
	Attachments []string
	Template    EmailTemplate
}

func (emailer *Emailer) Send() error {
	switch emailer.Provider {
	case EMAIL_PROVIDER_MAILGUN:
		return sendMessageToMailgun(emailer)
	case EMAIL_PROVIDER_SENDGRID:
		return sendMessageToSendgrid(emailer)
	case EMAIL_PROVIDER_SMTP:
		return sendMessageBySmtp(emailer)
	default:
		return errors.New("Mailer.Send(): invalid mail provider")
	}
}

func (emailer *Emailer) renderTemplate() (string, error) {
	tpl, err := template.New(emailer.Template.File).Funcs(emailer.Template.FuncMap).ParseFiles(emailer.Template.File)
	if err != nil {
		return "", err
	}
	buff := new(bytes.Buffer)
	if err := tpl.ExecuteTemplate(buff, emailer.Template.Name, emailer.Template.Data); err != nil {
		return "", err
	}
	return buff.String(), nil
}
